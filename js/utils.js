async function checkForPeripherals() {
    let bothDevices;
    let microphone = null;
    let webcam = null;

    bothDevices = await navigator.mediaDevices.getUserMedia({audio: true, video: true})
        .catch(function (e) {
            console.log("There was a problem getting media devices: " + e.message);
        });

    if (bothDevices == null) {
        microphone = await checkForMicrophone();
        webcam = await checkForWebcam();
    }

    let peripherals = [bothDevices, microphone, webcam];

    return peripherals;
}

async function checkForMicrophone() {
    let stream = null;

    try {
        stream = await navigator.mediaDevices.getUserMedia({audio: true});
    } catch (e) {
        console.log("There was a problem getting user microphone: " + e.message);
    }

    return stream;
}

async function checkForWebcam() {
    let stream = null;

    try {
        stream = await navigator.mediaDevices.getUserMedia({video: true});
    } catch (e) {
        console.log("There was a problem getting user webcam: " + e.message);
    }

    return stream;
}
