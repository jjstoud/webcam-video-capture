# Webcam Video Capture README

## Purpose
To capture audio and video from a local webcam, show preview of video after recording, and allow downloading the video as a mp4 file. Additionally, uploading directly to Dropbox has been integrated, but requires providing an access key on line 18 in the `main.html` file.

## Assumptions
- The user has an attached webcam with audio capabilities.
- A mp4 file is an acceptable format for the output file.

## Testing Performed
- The testing was done on a MacBook Pro with an integrated webcam.
- The longest video recorded for testing was just under 20 minutes.
- The largest file uploaded to Dropbox was the above test file (83.7 MB).

## Other Notes
- This was done as a small project for fun.
- After downloading the video and playing it in VLC media player, the video had no timestamps (still played back, though). After processing the video with HandBrake (https://handbrake.fr/), it showed timestamps as expected.

## Steps to Integrate with Dropbox
1. Sign in to Dropbox and navigate to the following page: https://www.dropbox.com/developers
2. In the upper right-hand corner, click the `App Console` button to get to your apps.
3. From the `App Console`, click the `Create app` button.
4. On the `Create app` screen, you should see the following steps:
    1. Choose an API (you'll want to select `Dropbox API` here)
    2. Choose the type of access you need (you'll want to select `App folder` here)
    3. Name your app (you name your app anything you want)
5. After filling out the above information, click the `Create app` button.
6. You should now be on a page showing details (ex. status, permission type, etc.) about your created Dropbox app. Look for the section on `OAuth 2` and check the `Generated access token` bit. You should see the option to `Generate` an access token here. Do that.
7. Now that you have a generated access token, copy that text and replace the value of line 18 (including the brackets) in the `main.html` file of this project with your copied access token.
8. Now you should be clear to open `main.html` in your browser and after recording your video, click the `Upload to Dropbox` button to upload the video (NOTE: When you upload to Dropbox, check the `Apps` folder for the app you created in steps 3-5. The uploads will be in that folder.).
